package main.application;

import main.MockApis.MockApi;
import main.entities.Post;

import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * Dummy service
 */
public class Application {
    public static void main(String[] args) {
        // https://5dc868f2672b6e001426b57f.mockapi.io/dummy-api
        final var mockApi = new MockApi("http://localhost:3000", "posts");
        final var postsRaw = mockApi.getAll(Post[].class);

        postsRaw
                .thenAcceptAsync((body) -> {
                    final var posts = Arrays.stream(body)
                            .filter(post -> post.id < 5)
                            .collect(Collectors.toList());

                    for (Post post : posts) {
                        System.out.println(post.title);
                    }

                    System.exit(0);
                })
                .exceptionallyAsync((err) -> {
                    System.out.println(String.format("error -> %s", err.toString()));
                    System.exit(1);
                    return null;
                });

        System.out.println("first");
    }
}
