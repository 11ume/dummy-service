package main.MockApis;

import com.google.gson.Gson;
import kong.unirest.Unirest;

import java.util.concurrent.CompletableFuture;

public class MockApi {
    private String URL;
    private String endPoint;

    public MockApi(String URL, String endPoint) {
        this.URL = URL;
        this.endPoint = endPoint;
    }

    /**
     * Return only post by you id
     * @param id id of any post
     * @param obj class to convert in object
     */
    public <T> CompletableFuture<T> getOne(String id, Class<T> obj) {
        var req = Unirest.get(String.format("%s/%s/%s", this.URL, this.endPoint, id));
        var body = req.asStringAsync();
        return body.thenApplyAsync((result) -> jsonToObject(result.getBody(), obj));
    }

    /**
     * Return all available posts
     * @param obj class to convert in object
     */
    public <T> CompletableFuture<T[]> getAll(Class<T[]> obj) {
        var req = Unirest.get(String.format("%s/%s", this.URL, this.endPoint));
        var body = req.asStringAsync();
        return body.thenApplyAsync((result) -> jsonToObject(result.getBody(), obj));
    }

    private <T> T jsonToObject(String body, Class<T> obj) {
        return new Gson().fromJson(body, obj);
    }
}
