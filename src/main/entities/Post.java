package main.entities;

public class Post {
    public int id;
    public String objectId;
    public String userId;
    public String title;
    public boolean completed;
}
